/**
 * Sample entry page script.
 */

goog.require('foo.SampleClass');


/**
 * The main method.
 */
var main = function() {
  var c = new foo.SampleClass();
  c.say();
};

main();
