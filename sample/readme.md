Sample Application
==================

This file can be used as a sample project readme.

These files are a simple sample application on top of the scaffolding. Update
this readme with your project details and replace application files in `src` as
needed.

--------------------------------------------------------------------------------

WARNING: Deploying the sample using the `was sample` command could overwrite or
destroy existing files in the scaffolding. This sample should only be used on
empty scaffolding in order to learn the structure of the scaffolding.

--------------------------------------------------------------------------------

This project is powered by Web Application Scaffolding.

Copyright (c) 2013 Jordon Mears.

Web Application Scaffolding is made available under the MIT license.
<http://opensource.org/licenses/MIT>
