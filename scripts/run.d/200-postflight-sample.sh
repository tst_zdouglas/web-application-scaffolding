#!/bin/bash
# Postflight example.
#
# This is just an empty stub demonstrating a script that will be executed
# after the main action. You should be able to add your customizations to the
# workflow here.
#
# Copyright (c) 2013 Jordon Mears.
#
# Web Application Scaffolding is made available under the MIT license.
# <http://opensource.org/licenses/MIT>
